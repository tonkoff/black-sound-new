IF NOT EXISTS ( SELECT [Name] FROM sys.databases WHERE [name] = 'blacksound' )
BEGIN
    CREATE DATABASE [blacksound]
END
GO

USE [blacksound]
GO

IF NOT EXISTS ( SELECT * FROM sysobjects where Name='users' and xtype='U')
    CREATE TABLE dbo.users (
        [Id] INT NOT NULL PRIMARY KEY IDENTITY,
        [Email] VARCHAR(50) NOT NULL,
        [Password] VARCHAR(50) NOT NULL,
        [Display_name] VARCHAR(50) NOT NULL,
        [Is_administrator] INT NOT NULL)
GO

IF NOT EXISTS ( SELECT * FROM sysobjects where Name='songs' and xtype='U')
    CREATE TABLE dbo.songs (
        [Id] INT NOT NULL PRIMARY KEY IDENTITY,
        [Title] VARCHAR(50) NOT NULL,
        [Artist_name] VARCHAR(50) NOT NULL,
        [Year] VARCHAR(50) NOT NULL)
         
GO

IF NOT EXISTS ( SELECT * FROM sysobjects where Name='playlist_songs' and xtype='U')
    CREATE TABLE dbo.playlist_songs (
        [Id] INT NOT NULL PRIMARY KEY IDENTITY,
        [Playlist_id] VARCHAR(50) NOT NULL,
        [Song_id] INT NOT NULL)
       
         
GO

IF NOT EXISTS ( SELECT * FROM sysobjects where Name='playlist' and xtype='U')
    CREATE TABLE dbo.playlist (
        [Id] INT NOT NULL PRIMARY KEY IDENTITY,
        [Name] VARCHAR(50) NOT NULL,
        [Description] VARCHAR(50) NOT NULL,
        [Is_public] INT NOT NULL,
        [User_id] INT NOT NULL)
       
         
GO

IF NOT EXISTS(SELECT Email FROM dbo.users WHERE EMAIL = 'admin@admin.com')
    
  INSERT INTO users (Email, Password, Display_name, Is_administrator) VALUES ('admin@admin.com', 'password', 'Nasko' , 1)

GO  

IF NOT EXISTS(SELECT Name FROM dbo.playlist WHERE Name = 'All Songs')

 INSERT INTO playlist(Name, Description, Is_public, User_id) VALUES ('All Songs', 'All songs playlist', 1 , 1)

GO
