﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Song
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Artist_namne { get; set; }

        public string Year { get; set; }
    }
}