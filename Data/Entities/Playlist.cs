﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Playlist
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Is_public { get; set; }

        public int User_id { get; set; }
    }
}