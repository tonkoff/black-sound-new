﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class SharedPlaylist
    {
        public int Id { get; set; }

        public int Shared_User_id { get; set; }

        public int Playlist_id { get; set; }
    }
}