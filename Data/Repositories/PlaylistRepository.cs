﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using System.Data.SqlClient;
using System.Data;

namespace Data.Repositories
{
    public class PlaylistRepository
    {
        private readonly string connectionString;

        public PlaylistRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Playlist> GetAll()
        {
            List<Playlist> resultSet = new List<Playlist>();

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();

                command.CommandText = @"SELECT * FROM playlist ";

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new Playlist()
                        {
                            Id = (int)reader["Id"],
                            Name = (string)reader["Name"],
                            Description = (string)reader["Description"],
                            Is_public = (int)reader["Is_public"],
                            User_id = (int)reader["User_id"],
                        });
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            return resultSet;
        }

        public void Insert(Playlist playlist)
        {
            IDbConnection connection = new SqlConnection(connectionString);

            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"INSERT INTO playlist (Name, Description, Is_public, User_id) VALUES (@Name,@Description,@Is_public,@User_id)";

            IDbDataParameter parameter = command.CreateParameter();
            parameter = command.CreateParameter();
            parameter.ParameterName = "@Name";
            parameter.Value = playlist.Name;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Description";
            parameter.Value = playlist.Description;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Is_public";
            parameter.Value = playlist.Is_public;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@User_id";
            parameter.Value = playlist.User_id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Update(Playlist playlist)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"UPDATE playlist SET Name=@Name,Description=@Description,Is_public=@Is_public,User_id=@User_id WHERE Id=@id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = playlist.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Name";
            parameter.Value = playlist.Name;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Description";
            parameter.Value = playlist.Description;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Is_public";
            parameter.Value = playlist.Is_public;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@User_id";
            parameter.Value = playlist.User_id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Delete(Playlist playlist)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();
            command.CommandText = @"DELETE FROM playlist WHERE Id=@id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = playlist.Id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public Playlist PlaylistGet(int id)
        {
            Playlist playlist = new Playlist();

            IDbConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM playlist WHERE Id = @Id";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@Id";
                parameter.Value = id;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        playlist.Id = (int)reader["Id"];
                        playlist.Name = (string)reader["Name"];
                        playlist.Description = (string)reader["Description"];
                        playlist.Is_public = (int)reader["Is_public"];
                        playlist.User_id = (int)reader["User_id"];
                    }
                }
                return playlist;
            }
            finally
            {
                connection.Close();
            }
        }

        public void AddSongToPlaylist(Song song, Playlist playlist)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"INSERT INTO playlist_songs (Playlist_id, Song_id) VALUES (@Playlist_id,@Song_id)";

            IDbDataParameter parameter = command.CreateParameter();
            parameter = command.CreateParameter();
            parameter.ParameterName = "@Playlist_id";
            parameter.Value = playlist.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Song_id";
            parameter.Value = song.Id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void RemoveSongFromPlaylist(Song song, Playlist playlist)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"DELETE FROM playlist_songs WHERE Playlist_id=@playlist_id AND Song_id=@song_id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@playlist_id";
            parameter.Value = playlist.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@song_id";
            parameter.Value = song.Id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public List<Song> ShowAllSongsFromPlaylist(Playlist playlist)
        {
            List<Song> resultSet = new List<Song>();

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();

                command.CommandText = @"Select
s.*,
p.Playlist_id as playlist_id,
p.Song_id as song_id
FROM songs as s
JOIN playlist_songs as p ON s.id = p.song_id
WHERE Playlist_id = @playlist_id";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@playlist_id";
                parameter.Value = playlist.Id;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new Song()
                        {
                            Id = (int)reader["Id"],
                            Title = (string)reader["Title"],
                            Artist_namne = (string)reader["Artist_name"],
                            Year = (string)reader["Year"],
                        });
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            return resultSet;
        }
    }
}