﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using System.Data.SqlClient;
using System.Data;

namespace Data.Repositories
{
    public class UserRepository
    {
        private readonly string connectionString;

        public UserRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<User> GetAll()
        {
            List<User> resultSet = new List<User>();

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();

                command.CommandText = @"SELECT * FROM users ";

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new User()
                        {
                            Id = (int)reader["Id"],
                            Email = (string)reader["Email"],
                            Password = (string)reader["Password"],
                            Display_name = (string)reader["Display_name"],
                            Is_administrator = (int)reader["Is_administrator"],
                        });
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            return resultSet;
        }

        public List<Playlist> GetUserPlaylists(int id)
        {
            List<Playlist> resultSet = new List<Playlist>();
            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM playlist WHERE User_id = @Id";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@Id";
                parameter.Value = id;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new Playlist()
                        {
                            Id = (int)reader["Id"],
                            Name = (string)reader["Name"],
                            Description = (string)reader["Description"],
                            Is_public = (int)reader["Is_public"],
                            User_id = (int)reader["User_id"],
                        });
                    }
                }

                return resultSet;
            }
            finally
            {
                connection.Close();
            }
        }

        public void Insert(User user)
        {
            IDbConnection connection = new SqlConnection(connectionString);

            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"INSERT INTO users (Email, Password, Display_name, Is_administrator) VALUES (@Email,@Password,@Display_name,@Is_administrator)";

            IDbDataParameter parameter = command.CreateParameter();
            parameter = command.CreateParameter();
            parameter.ParameterName = "@Email";
            parameter.Value = user.Email;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Password";
            parameter.Value = user.Password;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Display_name";
            parameter.Value = user.Display_name;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Is_administrator";
            parameter.Value = user.Is_administrator;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Update(User user)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"UPDATE users SET Email=@Email,Password=@Password,Display_name=@Display_name,Is_administrator=@Is_administrator WHERE Id=@id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = user.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Email";
            parameter.Value = user.Email;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Password";
            parameter.Value = user.Password;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Display_name";
            parameter.Value = user.Display_name;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Is_administrator";
            parameter.Value = user.Is_administrator;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Delete(User user)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();
            command.CommandText = @"DELETE FROM users WHERE Id=@id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = user.Id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public User UserGet(string Email)
        {
            User user = new User();

            IDbConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM users WHERE Email = @Email";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@Email";
                parameter.Value = Email;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        user.Id = (int)reader["Id"];
                        user.Email = (string)reader["Email"];
                        user.Password = (string)reader["Password"];
                        user.Display_name = (string)reader["Display_name"];
                        user.Is_administrator = (int)reader["Is_administrator"];
                    }
                }
                return user;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}