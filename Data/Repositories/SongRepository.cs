﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using System.Data.SqlClient;
using System.Data;

namespace Data.Repositories
{
    public class SongRepository
    {
        private readonly string connectionString;

        public SongRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Song> GetAll()
        {
            List<Song> resultSet = new List<Song>();

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();

                command.CommandText = @"SELECT * FROM songs ";

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new Song()
                        {
                            Id = (int)reader["Id"],
                            Title = (string)reader["Title"],
                            Artist_namne = (string)reader["Artist_name"],
                            Year = (string)reader["Year"]
                        }
                            );
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            return resultSet;
        }

        public void Insert(Song song)
        {
            IDbConnection connection = new SqlConnection(connectionString);

            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"INSERT INTO SONGS (Title, Artist_name, Year) VALUES (@Title,@Artist_name,@Year)";

            IDbDataParameter parameter = command.CreateParameter();
            parameter = command.CreateParameter();
            parameter.ParameterName = "@Title";
            parameter.Value = song.Title;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Artist_name";
            parameter.Value = song.Artist_namne;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Year";
            parameter.Value = song.Year;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Update(Song song)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"UPDATE songs SET Title=@Title,Artist_name=@Artist_name,Year=@Year WHERE Id=@id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = song.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Title";
            parameter.Value = song.Title;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Artist_name";
            parameter.Value = song.Artist_namne;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Year";
            parameter.Value = song.Year;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Delete(Song song)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();
            command.CommandText = @"DELETE FROM SONGS WHERE Id=@id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = song.Id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public Song SongGet(int id)
        {
            Song song = new Song();

            IDbConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM songs WHERE Id = @id";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@id";
                parameter.Value = id;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        song.Id = (int)reader["Id"];
                        song.Title = (string)reader["Title"];
                        song.Artist_namne = (string)reader["Artist_name"];
                        song.Year = (string)reader["Year"];
                    }
                }
                return song;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}