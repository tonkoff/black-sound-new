﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using System.Data.SqlClient;
using System.Data;

namespace Data.Repositories
{
    public class SharedPlaylistRepository
    {
        private readonly string connectionString;

        public SharedPlaylistRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Playlist> GetAll(int id)
        {
            List<Playlist> resultSet = new List<Playlist>();

            List<User> users = new List<User>();

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();

                command.CommandText = @"SELECT
p.Id, p.Name, p.Description, p.Is_public, p.User_id,
s.Shared_User_id as shared_user_id,
s.Playlist_id as Playlist_id,
s.User_id as Owner,
u.Email as User_email
FROM playlist as p
JOIN shared_playlists as s ON p.id = s.Playlist_id
JOIN users as u ON s.Shared_User_id = u.Id
WHERE s.User_id = @User_id;
 ";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@User_id";
                parameter.Value = id;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new Playlist()
                        {
                            Id = (int)reader["Id"],
                            Name = (string)reader["Name"],
                            Description = (string)reader["User_email"],
                            Is_public = (int)reader["Is_public"],
                            User_id = (int)reader["User_id"],
                        });
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            return resultSet;
        }

        public void Insert(Playlist playlist, User user, int id)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"INSERT INTO shared_playlists (Shared_user_id, Playlist_id, User_id) VALUES (@Shared_user_id,@Playlist_id,@User_id)";

            IDbDataParameter parameter = command.CreateParameter();
            parameter = command.CreateParameter();
            parameter.ParameterName = "@Shared_user_id";
            parameter.Value = user.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Playlist_id";
            parameter.Value = playlist.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@User_id";
            parameter.Value = id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Unshare(int id)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = connection.CreateCommand();

            command.CommandText = @"DELETE FROM shared_playlists WHERE Playlist_id=@playlist_id";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@playlist_id";
            parameter.Value = id;
            command.Parameters.Add(parameter);

            //parameter = command.CreateParameter();
            //parameter.ParameterName = "@Shared_User_id";
            //parameter.Value = user.Id;
            //command.Parameters.Add(parameter);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public List<Playlist> GetSharedPlaylists(int id)
        {
            List<Playlist> resultSet = new List<Playlist>();

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();

                command.CommandText = @"SELECT
 p.Name, p.Description,
s.Shared_User_id as shared_user_id,
s.Playlist_id as Playlist_id
FROM playlist as p
JOIN shared_playlists as s ON p.id = s.Playlist_id
Where Shared_User_id  = @User_id;  ";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@User_id";
                parameter.Value = id;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new Playlist()
                        {
                            Name = (string)reader["Name"],
                            Description = (string)reader["Description"],
                        });
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            return resultSet;
        }
    }
}