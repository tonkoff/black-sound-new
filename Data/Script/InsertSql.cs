﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;

namespace Data.Repositories
{
    public class InsertSQL
    {
        public void SQL()
        {
            SqlConnection connection = new SqlConnection(@"Server = DESKTOP-FTKQ95G\SQLEXPRESS; Initial Catalog = blacksound; Integrated Security = true;");

            try
            {
                connection.Open();

                string script = File.ReadAllText(@"C:\Users\A.Tonkov\Desktop\BlackSound\Data\Sql\script.sql");

                Server server = new Server(new ServerConnection(connection));

                server.ConnectionContext.ExecuteNonQuery(script);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}