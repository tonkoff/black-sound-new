﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Repositories;

namespace BlackSound.ViewControllers
{
    public class PlaylistController : BaseController
    {
        internal void UpdatePlaylist()
        {
            List<Playlist> myPlaylists = new List<Playlist>();

            myPlaylists = userRepository.GetUserPlaylists(currentUser.Id);

            Console.WriteLine("My playlists");
            Console.WriteLine("-------------------------");

            foreach (var myPlaylist in myPlaylists)
            {
                Console.WriteLine(myPlaylist.Id + "  " + myPlaylist.Name);
            }

            Console.WriteLine("--------------------------");
            Console.WriteLine("Please choose playlist to update");

            int PlaylistId = int.Parse(Console.ReadLine());

            Playlist playlistSelect = new Playlist();
            playlistSelect = playlistRepository.PlaylistGet(PlaylistId);

            if (playlistSelect.Name != null)
            {
                Console.WriteLine();
                Console.WriteLine("Please enter playlist name");
                playlistSelect.Name = Console.ReadLine();
                Console.WriteLine("Please enter playlist description");
                playlistSelect.Description = Console.ReadLine();
                Console.WriteLine("Playlist is public?");
                playlistSelect.Is_public = int.Parse(Console.ReadLine());

                playlistRepository.Update(playlistSelect);

                Console.WriteLine("Playlist Updated");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();
                PlaylistMenu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Playlist not found");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();
                UpdatePlaylist();
            }
        }

        internal void ViewSongsFromPlaylist()
        {
            Playlist playlist = new Playlist();
            Console.WriteLine("----------------------------");
            Console.WriteLine("Please select a playlist");
            int playlistId = int.Parse(Console.ReadLine());

            playlist = playlistRepository.PlaylistGet(playlistId);

            if (playlist.Name != null)
            {
                List<Song> songs = new List<Song>();

                songs = playlistRepository.ShowAllSongsFromPlaylist(playlist);

                Console.WriteLine("All Songs from " + playlist.Name + " playlist");
                Console.WriteLine("-------------------------------");

                foreach (var song in songs)
                {
                    Console.WriteLine(song.Title + "     " + song.Artist_namne + "    " + song.Year);
                }

                PlaylistMenu1();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Playlist not found");
                System.Threading.Thread.Sleep(1500);

                ViewSongsFromPlaylist();
            }
        }

        internal void DeleteSongFromPlaylist()
        {
            List<Playlist> myPlaylists = new List<Playlist>();

            myPlaylists = userRepository.GetUserPlaylists(currentUser.Id);

            Console.WriteLine("My playlists");
            Console.WriteLine("------------------------------");

            foreach (var myPlaylist in myPlaylists)
            {
                Console.WriteLine(myPlaylist.Id + "  " + myPlaylist.Name);
            }

            Console.WriteLine("---------------------");

            Playlist playlist = new Playlist();
            Song song = new Song();

            Console.WriteLine("Please choose a playlist");

            int playlistId = int.Parse(Console.ReadLine());

            playlist = playlistRepository.PlaylistGet(playlistId);

            if (playlist.Name != null)
            {
                Console.WriteLine("----------------------------");
                Console.WriteLine("All songs from playlist");

                songController.ShowAllSongs(playlist);

                Console.WriteLine("-----------------------------");
                Console.WriteLine("Choose a song to delete");
                int songId = int.Parse(Console.ReadLine());

                song = songRepository.SongGet(songId);

                if (song.Title != null)
                {
                    playlistRepository.RemoveSongFromPlaylist(song, playlist);

                    Console.WriteLine("Song removed");

                    System.Threading.Thread.Sleep(1500);

                    Console.Clear();

                    PlaylistMenu();
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Song not found");
                    System.Threading.Thread.Sleep(1500);

                    Console.Clear();
                    DeleteSongFromPlaylist();
                }
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Playlist not found");
                System.Threading.Thread.Sleep(1500);

                Console.Clear();
                DeleteSongFromPlaylist();
            }
        }

        internal void ShowMyPlaylists()
        {
            List<Playlist> myPlaylists = new List<Playlist>();

            myPlaylists = userRepository.GetUserPlaylists(currentUser.Id);

            foreach (var myPlaylist in myPlaylists)
            {
                Console.WriteLine(myPlaylist.Id + "  " + myPlaylist.Name);
            }

            Console.WriteLine("---------------------");

            PlaylistMenu1();
        }

        internal void ShowPlaylists()
        {
            List<Playlist> playlists = new List<Playlist>();
            List<Playlist> sharedPplaylists = new List<Playlist>();

            playlists = playlistRepository.GetAll();

            Console.WriteLine("All playlists");
            Console.WriteLine("---------------------------");

            foreach (var playlist in playlists)
            {
                if (playlist.Is_public == 1 || playlist.User_id == currentUser.Id)
                {
                    Console.WriteLine(playlist.Id + "  " + playlist.Name);
                }
            }

            sharedPplaylists = sharedPlaylistRepository.GetSharedPlaylists(currentUser.Id);

            Console.WriteLine("------------------------------");
            Console.WriteLine("Shared Playlists");
            Console.WriteLine("-------------------------------");
            foreach (var sharedPplaylist in sharedPplaylists)
            {
                Console.WriteLine(sharedPplaylist.Id + "  " + sharedPplaylist.Name + " " + sharedPplaylist.Description);
            }

            Console.WriteLine("-------------------------------------");

            PlaylistMenu1();
        }

        internal void AddNewPlaylist()
        {
            Playlist playlist = new Playlist();

            Console.WriteLine("Please enter playlist name");
            playlist.Name = Console.ReadLine();
            Console.WriteLine("Please enter playlist description");
            playlist.Description = Console.ReadLine();
            Console.WriteLine("Is public");
            playlist.Is_public = int.Parse(Console.ReadLine());
            playlist.User_id = currentUser.Id;

            playlistRepository.Insert(playlist);

            ShowPlaylists();
            Console.WriteLine();
            Console.WriteLine("Playlist Added");

            System.Threading.Thread.Sleep(1500);
            Console.Clear();
            PlaylistMenu();
        }

        internal void DeletePlaylist()
        {
            List<Playlist> playlists = new List<Playlist>();

            playlists = userRepository.GetUserPlaylists(currentUser.Id);

            Console.WriteLine();
            Console.WriteLine("My playlists");
            Console.WriteLine("-----------------------");

            foreach (var item in playlists)
            {
                Console.WriteLine(item.Id + "  " + item.Name);
            }

            Playlist playlist = new Playlist();

            Console.WriteLine();
            Console.WriteLine("Please select playlist to delete");
            int playlistId = int.Parse(Console.ReadLine());

            playlist = playlistRepository.PlaylistGet(playlistId);

            if (playlist.Name != null)
            {
                playlistRepository.Delete(playlist);
                Console.WriteLine();
                Console.WriteLine("Playlist was deleted");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();
                PlaylistMenu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Playlist not found");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();
                DeletePlaylist();
            }
        }

        internal void AddSongToPlaylist()
        {
            List<Song> songs = new List<Song>();

            songs = songRepository.GetAll();

            Console.WriteLine("All songs");
            Console.WriteLine("-------------");
            Console.WriteLine();
            foreach (var song in songs)
            {
                Console.WriteLine(song.Id + " " + song.Title);
            }

            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("All playlists");
            Console.WriteLine();
            List<Playlist> playlists = new List<Playlist>();

            playlists = userRepository.GetUserPlaylists(currentUser.Id);

            foreach (var playlist in playlists)
            {
                if (playlist.Is_public == 1 || playlist.User_id == currentUser.Id)
                {
                    Console.WriteLine(playlist.Id + "  " + playlist.Name);
                }
            }

            Song songOfChoise = new Song();
            Console.WriteLine();

            Console.WriteLine("Please select song");
            int songId = int.Parse(Console.ReadLine());
            songOfChoise = songRepository.SongGet(songId);

            if (songOfChoise.Title == null)
            {
                Console.WriteLine();
                Console.WriteLine("Song not found");
                System.Threading.Thread.Sleep(1500);
                AddSongToPlaylist();
            }

            Console.WriteLine("---------------------------");

            Console.WriteLine("Please select playlist");
            int namepl = int.Parse(Console.ReadLine());

            Playlist playlistChoice = new Playlist();

            playlistChoice = playlistRepository.PlaylistGet(namepl);

            List<Song> playlistSongsCheck = new List<Song>();

            playlistSongsCheck = playlistRepository.ShowAllSongsFromPlaylist(playlistChoice);

            foreach (var item in playlistSongsCheck)
            {
                if (item.Id == songOfChoise.Id)
                {
                    Console.WriteLine("Song already added");
                    System.Threading.Thread.Sleep(2000);
                    Console.Clear();
                    PlaylistMenu();
                    break;
                }
            }

            if (playlistChoice.Name != null)
            {
                playlistRepository.AddSongToPlaylist(songOfChoise, playlistChoice);

                Console.WriteLine("Song Added");

                System.Threading.Thread.Sleep(1500);

                Console.Clear();

                PlaylistMenu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Playlist not found");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();

                AddSongToPlaylist();
            }
        }
    }
}