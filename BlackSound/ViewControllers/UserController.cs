﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Repositories;

namespace BlackSound.ViewControllers
{
    public class UserController : BaseController
    {
        internal void DeleteUser()
        {
            List<User> users = new List<User>();

            users = userRepository.GetAll();

            Console.WriteLine("ALL users");
            Console.WriteLine("------------------------------");

            foreach (var item in users)
            {
                Console.WriteLine(item.Email + "  " + item.Display_name);
            }

            User user = new User();

            Console.WriteLine("--------------------------------");
            Console.WriteLine("Please select user to delete by email");
            string userChoise = Console.ReadLine();

            user = userRepository.UserGet(userChoise);

            if (user.Email != null)
            {
                userRepository.Delete(user);

                Console.WriteLine("User deleted");

                System.Threading.Thread.Sleep(1500);

                Console.Clear();

                MainMenu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("User not found");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();
                DeleteUser();
            }
        }

        internal void AddUser()
        {
            User newuser = new User();

            Console.WriteLine("----------------------------");
            Console.WriteLine("Please enter an email");
            newuser.Email = Console.ReadLine();
            Console.WriteLine("Please enter password");
            newuser.Password = Console.ReadLine();
            Console.WriteLine("Please enter your name");
            newuser.Display_name = Console.ReadLine();
            Console.WriteLine("Is admin");
            newuser.Is_administrator = int.Parse(Console.ReadLine());

            newuser.Password = GetHash(newuser.Password);

            User savedUser = new User();

            savedUser = userRepository.UserGet(newuser.Email);

            if (savedUser.Email == null)
            {
                userRepository.Insert(newuser);

                Console.WriteLine("User Added");

                System.Threading.Thread.Sleep(1500);

                Console.Clear();

                MainMenu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Someone else uses this email");
                System.Threading.Thread.Sleep(1500);
                AddUser();
            }
        }
    }
}