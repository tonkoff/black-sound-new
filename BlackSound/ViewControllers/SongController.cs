﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Repositories;

namespace BlackSound.ViewControllers
{
    public class SongController : BaseController
    {
        internal void SongAdd()
        {
            Song song = new Song();

            Console.WriteLine("Please enter song title");
            song.Title = Console.ReadLine();
            Console.WriteLine("Please eneter song artist");
            song.Artist_namne = Console.ReadLine();
            Console.WriteLine("Please enter song year");
            song.Year = Console.ReadLine();

            songRepository.Insert(song);
            Console.WriteLine("Song Added");
            System.Threading.Thread.Sleep(1500);
            Console.Clear();
            MainMenu();
        }

        internal void DeleteSong()
        {
            List<Song> songs = new List<Song>();

            songs = songRepository.GetAll();

            Console.WriteLine("All songs");
            Console.WriteLine("-------------");
            Console.WriteLine();
            foreach (var song in songs)
            {
                Console.WriteLine(song.Id + "  " + song.Title);
            }

            Console.WriteLine("----------------------");
            Console.WriteLine("Please select a song to delete");
            int songId = int.Parse(Console.ReadLine());

            Song songDelete = new Song();

            songDelete = songRepository.SongGet(songId);

            if (songDelete.Title != null)
            {
                songRepository.Delete(songDelete);

                Console.WriteLine("Song Deleted");

                System.Threading.Thread.Sleep(1500);

                Console.Clear();

                MainMenu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Song not found");
                System.Threading.Thread.Sleep(1500);

                Console.Clear();

                DeleteSong();
            }
        }

        internal void ViewAllAvaiableSongs()
        {
            List<Song> songs = new List<Song>();

            songs = songRepository.GetAll();

            Console.WriteLine("All songs");
            Console.WriteLine("------------------------------");

            foreach (var song in songs)
            {
                Console.WriteLine(song.Title + " " + song.Artist_namne + " " + song.Year);
            }

            Console.WriteLine("------------------------------");

            MainMenu();
        }

        internal void ShowAllSongs(Playlist playlist)
        {
            List<Song> songs = new List<Song>();

            songs = playlistRepository.ShowAllSongsFromPlaylist(playlist);

            foreach (var song in songs)
            {
                Console.WriteLine(song.Id + " " + song.Title);
            }
            Console.WriteLine();
        }

        internal void UpdateSong()
        {
            List<Song> songs = new List<Song>();

            songs = songRepository.GetAll();

            Console.WriteLine("All songs");
            Console.WriteLine("------------------------------");

            foreach (var song in songs)
            {
                Console.WriteLine(song.Id + "  " + song.Title + " " + song.Artist_namne + " " + song.Year);
            }

            Song songUpdate = new Song();

            Console.WriteLine("------------------------------");

            Console.WriteLine("Please choose a song to update");
            int id = int.Parse(Console.ReadLine());

            songUpdate = songRepository.SongGet(id);

            if (songUpdate.Title != null)
            {
                Console.WriteLine("Please enter song title");
                songUpdate.Title = Console.ReadLine();
                Console.WriteLine("Please enter song artist");
                songUpdate.Artist_namne = Console.ReadLine();
                Console.WriteLine("Please select song year");
                songUpdate.Year = Console.ReadLine();

                songRepository.Update(songUpdate);

                Console.WriteLine("Song updated");

                System.Threading.Thread.Sleep(1500);

                Console.Clear();

                MainMenu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Song not found");
                System.Threading.Thread.Sleep(1500);

                Console.Clear();
                UpdateSong();
            }
        }
    }
}