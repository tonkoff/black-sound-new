﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Repositories;

namespace BlackSound.ViewControllers
{
    public class ShareController : BaseController
    {
        internal void UnSharePlaylist()
        {
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Please select id to delete");

            int id = int.Parse(Console.ReadLine());

            sharedPlaylistRepository.Unshare(id);

            Console.WriteLine("Playlist was unshared");

            System.Threading.Thread.Sleep(1500);
            Console.Clear();
            ShareMenu();
        }

        internal void ShowSharedPlaylists()
        {
            List<Playlist> playlists = new List<Playlist>();

            playlists = sharedPlaylistRepository.GetAll(currentUser.Id);

            Console.WriteLine();
            Console.WriteLine("Shared Playlist");
            Console.WriteLine("-----------------------------");

            foreach (var playlist in playlists)
            {
                Console.WriteLine(playlist.Id + " " + playlist.Name + " shared with " + playlist.Description);
            }

            ShareMenu();
        }

        internal void ShareAPlaylist()
        {
            List<Playlist> playlists = new List<Playlist>();

            playlists = userRepository.GetUserPlaylists(currentUser.Id);

            Console.WriteLine();
            Console.WriteLine("Playlist to share");
            Console.WriteLine("---------------------------");

            foreach (var item in playlists)
            {
                Console.WriteLine(item.Id + "  " + item.Name);
            }

            Console.WriteLine("---------------------");
            Console.WriteLine("Please select a playlist");
            Playlist playlist = new Playlist();

            int playlistId = int.Parse(Console.ReadLine());

            playlist = playlistRepository.PlaylistGet(playlistId);

            List<User> users = new List<User>();

            users = userRepository.GetAll();

            Console.WriteLine("Users");
            Console.WriteLine("---------------------");

            foreach (var item in users)
            {
                Console.WriteLine(item.Id + "  " + item.Email);
            }

            User user = new User();

            Console.WriteLine("---------------------------");
            Console.WriteLine("Please select an user email");

            string userSelect = Console.ReadLine();

            user = userRepository.UserGet(userSelect);

            if (playlist.Name != null && user.Email != null)
            {
                sharedPlaylistRepository.Insert(playlist, user, currentUser.Id);

                Console.WriteLine("Playlist Shared");

                System.Threading.Thread.Sleep(1500);
                Console.Clear();
                ShareMenu();
            }
            else
            {
                Console.WriteLine("Playlist or user not found");
                ShareAPlaylist();
            }
        }
    }
}