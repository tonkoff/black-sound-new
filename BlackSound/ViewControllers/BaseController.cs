﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Data.Entities;
using Data.Repositories;

namespace BlackSound.ViewControllers
{
    public class BaseController
    {
        protected static string connectionString = @"Server=DESKTOP-FTKQ95G\SQLEXPRESS;Initial Catalog=blacksound; Integrated Security = true;";

        protected UserRepository userRepository = new UserRepository(connectionString);
        protected PlaylistRepository playlistRepository = new PlaylistRepository(connectionString);
        protected SongRepository songRepository = new SongRepository(connectionString);
        protected SharedPlaylistRepository sharedPlaylistRepository = new SharedPlaylistRepository(connectionString);

        public static UserController userController = new UserController();
        public static SongController songController = new SongController();
        public static PlaylistController playlistController = new PlaylistController();
        public static ShareController shareController = new ShareController();

        protected static User currentUser = new User();

        public void MainMenu()
        {
            try
            {
                Console.WriteLine("Welcome " + currentUser.Display_name);
                Console.Write("======================================= \n");
                Console.Write("   [1]  Playlists                    \n");
                Console.Write("   [2]  Add Song                             \n");
                Console.Write("   [3]  Delete Song                \n");
                Console.Write("   [4]  View All Songs                 \n");
                Console.Write("   [5]  Update Song                  \n");
                Console.Write("   [6]  Add User                 \n");
                Console.Write("   [7]  Delete  User                             \n");
                Console.Write("   [8]  Close Application                    \n\n\n");

                Console.Write("Please Input Number [1-8] then Press Enter to Begin : ");

                int selection = Convert.ToInt32(Console.ReadLine());

                Mainmenu mainmenu = (Mainmenu)selection;

                switch (mainmenu)
                {
                    case Mainmenu.Playlists:
                        Console.Clear();
                        PlaylistMenu();
                        break;

                    case Mainmenu.AddSong:
                        songController.SongAdd();
                        break;

                    case Mainmenu.DeleteSong:
                        songController.DeleteSong();
                        break;

                    case Mainmenu.ViewAllSongs:
                        songController.ViewAllAvaiableSongs();
                        break;

                    case Mainmenu.UpdateSong:
                        songController.UpdateSong();
                        break;

                    case Mainmenu.AddUser:
                        userController.AddUser();
                        break;

                    case Mainmenu.DeleteUser:
                        userController.DeleteUser();
                        break;

                    case Mainmenu.CloseApplication:
                        Console.WriteLine("Press Enter key and Goodbye!");
                        Console.ReadLine();
                        break;

                    default:
                        Console.WriteLine("Invalid input");
                        System.Threading.Thread.Sleep(1500);
                        Console.Clear();
                        MainMenu();
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Error Input!\n\n");
                Console.WriteLine("Please Enter key to go back to Main Menu...");
                Console.ReadLine();
                Console.Clear();
                MainMenu();
            }
        }

        public void ShareMenu()
        {
            try
            {
                Console.Write("======================================= \n");
                Console.Write("   [1]  Shared Playlists                    \n");
                Console.Write("   [2]  Share a Playlist                     \n");
                Console.Write("   [3]  Unshare Playlist                      \n");
                Console.Write("   [4]  Playlist Menu                          \n");

                Console.Write("Please Input Number [1-4] then Press Enter to Begin : ");

                int selection = Convert.ToInt32(Console.ReadLine());

                ShareMenuEnum shareMenuEnum = (ShareMenuEnum)selection;

                switch (shareMenuEnum)
                {
                    case ShareMenuEnum.SharedPlaylists:

                        shareController.ShowSharedPlaylists();
                        break;

                    case ShareMenuEnum.ShareAPlaylist:
                        shareController.ShareAPlaylist();
                        break;

                    case ShareMenuEnum.UnsharePlaylist:
                        shareController.UnSharePlaylist();
                        break;

                    case ShareMenuEnum.PlaylistMenu:
                        PlaylistMenu();
                        break;

                    default:
                        Console.WriteLine("Invalid input");
                        System.Threading.Thread.Sleep(1500);
                        Console.Clear();
                        ShareMenu();
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Error Input!\n\n");
                Console.WriteLine("Please Enter key to go back to Main Menu...");
                Console.ReadLine();
                Console.Clear();
                ShareMenu();
            }
        }

        public void PlaylistMenu()
        {
            try
            {
                Console.WriteLine("Welcome " + currentUser.Display_name);
                Console.Write("======================================= \n");
                Console.Write("   [1]  Show Playlists                    \n");
                Console.Write("   [2]  Show my Playlists                    \n");
                Console.Write("   [3]  Add new Playlist                    \n");
                Console.Write("   [4]  Delete Playlist                      \n");
                Console.Write("   [5]  Add Song to Playlist                \n");
                Console.Write("   [6]  Delete Song from Playlist             \n");
                Console.Write("   [7]  Update Playlist                        \n");

                if (currentUser.Is_administrator == 1)
                {
                    Console.Write("   [8]  Main  Menu                     \n\n");
                }

                Console.Write("   [9]  Share                         \n");
                Console.WriteLine();

                Console.Write("Please Input Number [1-8] then Press Enter to Begin : ");

                int selection = Convert.ToInt32(Console.ReadLine());

                PlaylistMenu playlistMenu = (PlaylistMenu)selection;

                switch (playlistMenu)
                {
                    case BlackSound.PlaylistMenu.ShowPlaylists:
                        playlistController.ShowPlaylists();
                        break;

                    case BlackSound.PlaylistMenu.ShowMyPlaylists:
                        playlistController.ShowMyPlaylists();
                        break;

                    case BlackSound.PlaylistMenu.AddNewPlaylist:
                        playlistController.AddNewPlaylist();
                        break;

                    case BlackSound.PlaylistMenu.DeletePlaylist:
                        playlistController.DeletePlaylist();
                        break;

                    case BlackSound.PlaylistMenu.AddSongToPlaylist:
                        playlistController.AddSongToPlaylist();
                        break;

                    case BlackSound.PlaylistMenu.DeleteSongFromPlaylist:
                        playlistController.DeleteSongFromPlaylist();
                        break;

                    case BlackSound.PlaylistMenu.UpdatePlaylist:
                        playlistController.UpdatePlaylist();
                        break;

                    case BlackSound.PlaylistMenu.MainMenu:
                        if (currentUser.Is_administrator == 1)
                        {
                            Console.Clear();
                            MainMenu();
                        }
                        else
                        {
                            Console.Clear();
                            PlaylistMenu();
                        }
                        break;

                    case BlackSound.PlaylistMenu.Share:
                        ShareMenu();
                        break;

                    default:
                        Console.WriteLine("Invalid input");
                        System.Threading.Thread.Sleep(1500);
                        Console.Clear();
                        PlaylistMenu();
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Input Invalid\n");
                Console.WriteLine("Please Enter key to go back to Playlist Menu...");
                Console.ReadLine();
                Console.Clear();
                PlaylistMenu();
            }
        }

        public void PlaylistMenu1()
        {
            try
            {
                Console.Write("======================================= \n");
                Console.Write("   [1]  View Songs from Playlists                    \n");
                Console.Write("   [2]  Playlist Menu                     \n\n\n");

                Console.Write("Please Input Number [1-2] then Press Enter to Begin : ");

                int selection = Convert.ToInt32(Console.ReadLine());

                PlaylistMenu1 playlistMenu1 = (PlaylistMenu1)selection;

                switch (playlistMenu1)
                {
                    case BlackSound.PlaylistMenu1.ViewSongsFromPlaylists:
                        playlistController.ViewSongsFromPlaylist();
                        break;

                    case BlackSound.PlaylistMenu1.PlaylistMenu:
                        PlaylistMenu();
                        break;

                    default:
                        Console.WriteLine("Invalid input");
                        System.Threading.Thread.Sleep(1500);
                        Console.Clear();
                        PlaylistMenu1();
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string GetHash(string input)
        {
            return string.Join("", (new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(input))).Select(x => x.ToString("X2")).ToArray());
        }
    }
}