﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Repositories;

namespace BlackSound.ViewControllers
{
    public class LoginController : BaseController
    {
        public LoginController()
        {
            InsertSQL insertSQL = new InsertSQL();
            insertSQL.SQL();
        }

        public void Login()
        {
            Console.WriteLine("Hello to the BlackSound Application");
            Console.WriteLine();
            Console.WriteLine("Please enter email to login");
            string email = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Please enter your password");
            string password = Console.ReadLine();

            password = GetHash(password);

            List<User> users = userRepository.GetAll();

            foreach (var user in users)
            {
                if (user.Email == email && user.Password == password)
                {
                    currentUser = user;

                    Console.WriteLine("Login Successful");
                    Console.WriteLine("Welcome " + user.Display_name);

                    break;
                }
            }

            if (currentUser.Email == null)
            {
                Console.WriteLine("Login failed");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();
                Login();
            }
            else if (currentUser.Is_administrator == 1)
            {
                Console.Clear();
                MainMenu();
            }
            else if (currentUser.Is_administrator == 0)
            {
                Console.Clear();
                PlaylistMenu();
            }
        }
    }
}