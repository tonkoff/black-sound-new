﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackSound
{
    public enum Mainmenu
    {
        Playlists = 1,
        AddSong,
        DeleteSong,
        ViewAllSongs,
        UpdateSong,
        AddUser,
        DeleteUser,
        CloseApplication
    }

    public enum PlaylistMenu
    {
        ShowPlaylists = 1,
        ShowMyPlaylists,
        AddNewPlaylist,
        DeletePlaylist,
        AddSongToPlaylist,
        DeleteSongFromPlaylist,
        UpdatePlaylist,
        MainMenu,
        Share
    }

    public enum PlaylistMenu1
    {
        ViewSongsFromPlaylists = 1,
        PlaylistMenu
    }

    public enum ShareMenuEnum
    {
        SharedPlaylists = 1,
        ShareAPlaylist,
        UnsharePlaylist,
        PlaylistMenu
    }
}