﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.ViewControllers;
using Data.Entities;
using Data.Repositories;

namespace BlackSound
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            LoginController loginController = new LoginController();

            loginController.Login();
        }
    }
}